/**
 * @author Tomasz Szeliga
 * @type {string}
 */
'use strict';

export const SOURCE = 'src'; // source of the application /src
export const APP_SOURCE = 'src/app'; // source of app /src/spp
export const APP_FILE = 'app'; // src/app/app.js
export const SERVE_PORT = '3000'; // for BrowserSync

