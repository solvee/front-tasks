/**
 * @author Tomasz Szeliga
 * @type {string}
 */
'use strict';

import gulp from 'gulp';
import jspm from 'jspm';
import rename from 'gulp-rename';
import serve from 'browser-sync';
import {SOURCE, SERVE_PORT} from './const';
import {PATHS} from './paths';

gulp.task('serve', function() {

    var socketOpts = {
        port: 8081, 
        path: SOURCE, 
        relativeTo: SOURCE
    };
    require('chokidar-socket-emitter')(socketOpts);
    serve({
        port: process.env.PORT || SERVE_PORT,
        open: false,
        files: [].concat(
            [PATHS.css],
            PATHS.html
        ),
        server: {
            baseDir: SOURCE,
            routes: {
                '/jspm.config.js': './jspm.config.js',
                '/jspm_packages': './jspm_packages'
            }
        }
    });
});
