/**
 * @author Tomasz Szeliga
 */

'use strict';

import path from 'path';
import {SOURCE, APP_SOURCE} from './const';

var resolveTo = function (resolvePath) {
		return function(glob) {
			glob = glob || '';
			return path.relative(SOURCE, path.resolve(path.join(SOURCE, resolvePath, glob)));
		}
	},
	resolveToApp = resolveTo(APP_SOURCE),
	paths = {
		css: resolveToApp('**/*.css'),
		jades: resolveToApp('**/*.jade'),
		html: [
			resolveToApp('**/*.html'),
			path.join(SOURCE, 'index.html')
		],
		blankTemplates: path.join(__dirname, 'generator', 'component/**/*.**'),
		dist: path.join(__dirname, '../dist/')
	};

export {resolveTo};
export {resolveToApp};
export const PATHS = paths;