/**
 * @author Tomasz Szeliga
 */
'use strict';

import gulp 							from 'gulp';
import jspm 							from 'jspm';
import path 							from 'path';
import rimraf 							from 'rimraf';
import ngAnnotate 						from 'gulp-ng-annotate';
import uglify 							from 'gulp-uglify';
import rename 							from 'gulp-rename';
import htmlreplace 						from 'gulp-html-replace';
import jade 							from 'gulp-jade';
import tplCache							from 'gulp-angular-templatecache'
import {SOURCE, APP_FILE} 				from './const';
import {PATHS, resolveTo, resolveToApp} from './paths';

var babel = require("gulp-babel");


gulp.task('export', function () {

	return gulp.src("src/app/app.js")
			.pipe(babel())
			.pipe(gulp.dest("dist"));
});