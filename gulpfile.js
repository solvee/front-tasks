/**
 * @author Tomasz Szeliga
 */
'use strict';

var babel = require("gulp-babel"),
    gulp  = require("gulp");

gulp.task('export', function () {
    return gulp.src("src/app/app.js")
        .pipe(babel())
        .pipe(gulp.dest("dist"));
});

